﻿using ru.sazan.csharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ru.sazan.csharp.Data
{
    public interface OrderManager
    {
        void PlaceOrder(Order item);
    }
}
