﻿using ru.sazan.csharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ru.sazan.csharp.Collections
{
    public delegate void AddItemNotification<T>(T item);

    public class GenericObservableList<T>:List<T>
    {
        public event AddItemNotification<T> OnItemAdded;

        public new void Add(T item)
        {
            base.Add(item);

            if(OnItemAdded != null)
                OnItemAdded(item);
        }
    }
}
