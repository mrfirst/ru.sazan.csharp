﻿using ru.sazan.csharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ru.sazan.csharp.Collections
{
    public class GenericObservableHashSet<T> : HashSet<T>
    {
        public event AddItemNotification<T> OnItemAdded;

        public GenericObservableHashSet(IEqualityComparer<T> comparer)
            :base(comparer)
        {

        }

        public new void Add(T item)
        {
            base.Add(item);

            if(OnItemAdded != null)
                OnItemAdded(item);
        }
    }
}
