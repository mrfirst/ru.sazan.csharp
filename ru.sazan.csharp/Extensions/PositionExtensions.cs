﻿using ru.sazan.csharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ru.sazan.csharp.Extensions
{
    public static class PositionExtensions
    {
        public static double GetPositionAmount(this IEnumerable<Trade> tradeList)
        {
            return tradeList.Sum(t => t.Amount);
        }
    }
}
