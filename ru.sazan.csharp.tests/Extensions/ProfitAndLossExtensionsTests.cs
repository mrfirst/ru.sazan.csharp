﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.csharp.Collections;
using ru.sazan.csharp.Models;
using ru.sazan.csharp.Extensions;
using ru.sazan.csharp.tests.Utility;

namespace ru.sazan.csharp.tests.Extensions
{
    [TestClass]
    public class ProfitAndLossExtensionsTests
    {
        private GenericObservableList<Trade> tradeList;

        [TestInitialize]
        public void Setup() 
        {
            tradeList = new GenericObservableList<Trade>();
        }

        [TestMethod]
        public void GetProfitAndLossPoints_test()
        {
            tradeList.FillListWithEvenBuyAndOddSale(10);
            Assert.AreEqual(5, tradeList.GetProfitAndLossPoints());
        }

    }
}
