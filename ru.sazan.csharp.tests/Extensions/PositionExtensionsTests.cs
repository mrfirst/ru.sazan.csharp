﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.csharp.tests.Utility;
using ru.sazan.csharp.Collections;
using ru.sazan.csharp.Models;
using ru.sazan.csharp.Extensions;

namespace ru.sazan.csharp.tests.Extensions
{
    [TestClass]
    public class PositionExtensionsTests
    {
        private GenericObservableList<Trade> tradeList;

        [TestInitialize]
        public void Setup()
        {
            tradeList = new GenericObservableList<Trade>();
        }
        [TestMethod]
        public void GetPositionAmount_test()
        {
            tradeList.FillListWithEvenBuyAndOddSale(11);
            Assert.AreEqual(1, tradeList.GetPositionAmount());
        }
    }
}
